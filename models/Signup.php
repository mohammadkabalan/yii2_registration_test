<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * Signup is the model behind the login form.
 *
 * @property-read User|null $user
 *
 */
class Signup extends ActiveRecord
{
    private $username ;
    private $email ;
    private $password ;
    private $phone ;
    private $image ;
    private $has_kid ;
    public $kid_name ;
    public $date_of_birth ;
    public $gender ;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['username', 'required'],
            ['email', 'required'],
            ['email', 'email'],
            ['password', 'required'],
            ['phone', 'required'],
            ['phone', 'string', 'max' => 20],
            ['password', 'string', 'min' => 8 ],
            ['password', 'match', 'pattern' => '/^(?=.*\d)(?=.*[a-zA-Z])(?=.*[\W_]).{6,}$/',
                'message' => 'Password must contain at least 6 numbers, one character, and one special character.'],
            [['image'], 'required', 'on' => 'create'],
            [['image'], 'file', 'skipOnError' => true,'extensions' => 'png, jpg, gif','maxSize' => 1024 * 1024],
            ['kid_name', 'string', 'max' => 100],
            ['date_of_birth', 'date', 'format' => 'php:Y-m-d'],
            ['gender', 'in', 'range' => ['Male', 'Female']],
            ['has_kid', 'boolean'],
            ['kid_name', 'string'],
        ];
    }
    public function attributes(): array
    {
        return [
            'id' ,
            'username',
            'email',
            'password',
            'phone' ,
            'image' ,
            'has_kid',
            'kid_name',
            'date_of_birth' ,
            'gender'
            // ... other attributes
        ];
    }
    public static function primaryKey()
    {
        return ['id'];
    }
}