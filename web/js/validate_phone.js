 // Function to validate and format the phone number
 function validatePhoneNumber() {
  var inputElement = document.getElementById('phoneNumber');
  var phoneNumber = inputElement.value;

  try {
      var phoneNumberObject = libphonenumber.parsePhoneNumberFromString(phoneNumber);
      
      if (phoneNumberObject && phoneNumberObject.isValid()) {
          // Valid phone number
          //alert('Valid phone number: ' + phoneNumberObject.formatInternational());
      } else {
          // Invalid phone number
          alert('Invalid phone number');
      }
  } catch (error) {
      console.error('Error validating phone number:', error);
  }
}

// Attach the validation function to the input field's change event
document.getElementById('phoneNumber').addEventListener('change', validatePhoneNumber);