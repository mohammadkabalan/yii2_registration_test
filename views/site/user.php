
<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap5\ActiveForm $form */
/** @var app\models\ContactForm $model */

use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;
use yii\captcha\Captcha;
use fedemotta\datatables\DataTables;

$this->title = 'User';

?>
<img src="/admin/uploads/1666099925989.jpg" width="50px" alt="">
<?=
DataTables::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'username',
        'email',
        'phone',
        [
            'attribute' => 'image',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::img(Yii::$app->request->baseUrl . '/img/uploads/' . $model->image, ['width' => '50']);
            },
        ],
        'has_kid'
    ],

    'clientOptions' => [
        "lengthMenu"=> [[20,-1], [20,Yii::t('app',"All")]],
        "info"=>false,
        "responsive"=>true,
        "dom"=> 'lfTrtip',
        "tableTools"=>[
            "aButtons"=> [
                [
                    "sExtends"=> "copy",
                    "sButtonText"=> Yii::t('app',"Copy to clipboard")
                ],[
                    "sExtends"=> "csv",
                    "sButtonText"=> Yii::t('app',"Save to CSV")
                ],[
                    "sExtends"=> "xls",
                    "oSelectorOpts"=> ["page"=> 'current']
                ],[
                    "sExtends"=> "pdf",
                    "sButtonText"=> Yii::t('app',"Save to PDF")
                ]
            ]
        ]
    ],
]);?>

