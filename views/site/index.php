<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap5\ActiveForm $form */
/** @var app\models\ContactForm $model */

use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;
use yii\captcha\Captcha;

$this->title = 'Sign up';

?>

<link rel='stylesheet'
      href='https://d33wubrfki0l68.cloudfront.net/css/68818763319cf667ed141dc03e654e8db3424772/assets/css/animate.min.css' />
<link href="https://fonts.googleapis.com/css?family=Work+Sans:600" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Overpass:300,400,600,700,800,900" rel="stylesheet">




<header class="text-white" style="background-color: #28304e">
    <?php if(yii::$app->session->hasFlash('message')):  ?>
       <?php $flashMessage = Yii::$app->session->getFlash('message'); ?>
       <?php
        if ($flashMessage !== null) {
        echo '<div class="alert alert-info alert-dismissible fade show" role="alert">';
            echo $flashMessage;
            echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
            echo '</div>';
        }
      ?>

    <?php endif; ?>
    <div class="container">
        <div class="row m-h-100">
            <div class="col-md-6 my-auto">
                <h1 class="display-4 animated bounceIn font-worksans">Hello</h1>
                <p class="animated fadeIn">Sign up and start your journey with us let's Go</p>
                <p><a href="#" data-toggle="modal" data-target="#demoModal"
                      class="btn animated fadeIn btn-cta btn-cstm-light">Sign up</a></p>
            </div>
            <div class="text-center m-auto col-md-6 animated zoomIn">
                <p>
                    <img class="mw-100"
                         src="https://d33wubrfki0l68.cloudfront.net/45878c3fb0aed0317f92b2c2a5c9b708c52905e0/c0bcf/assets/img/cherry-done.png"
                         alt="">
                </p>
            </div>
        </div>
    </div>
</header>

<!-- Modal -->
<div class="modal fade auto-off" id="demoModal" tabindex="-1" role="dialog" aria-labelledby="demoModal"
     aria-hidden="true">
    <div class="modal-dialog animated zoomInDown modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="container-fluid">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row">
                    <div class="col-md-12 m-h-20 bg-img rounded-left"
                         style="background-image: url('https://images.unsplash.com/photo-1525097596740-cb2a70e07e3b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80')">
                    </div>
                    <div class="col-md-12 text-center py-5 px-sm-5 ">
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                        <?= $form->field($model, 'username') ?>
                        <?= $form->field($model, 'email') ?>
                        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'id'=>"phoneNumber"]) ?>

                        <?= $form->field($model, 'image')->fileInput() ?>


                        <?= $form->field($model, 'has_kid')->checkbox(['id' => 'kids']) ?>


                            <div class="row-kids" style="display: none;">
                                <div class="column_kids">
                                    <div class="mb-3 row">
                                        <?= $form->field($model, 'kid_name')->textInput(['maxlength' => true]) ?>

                                    </div>
                                </div>
                                <div class="column_kids">
                                    <div class="mb-3 row">
                                         <?= $form->field($model, 'date_of_birth')->textInput(['type' => 'date']) ?>

                                    </div>

                                </div>
                                <div class="column_kids">

                                    <div class="mb-3 row">
                                            <?= $form->field($model, 'gender')->dropDownList(['Male' => 'Male', 'Female' => 'Female']) ?>
                                    </div>

                                </div>
                            </div>
                            <!-- Submit Button -->
                             <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>


                        <?php ActiveForm::end(); ?>


                            <!-- <button type="submit" class="btn btn-cstm-dark btn-block btn-cta" data-dismiss="modal" aria-label="Close">close modal</button> -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

